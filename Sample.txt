index.ts
---------
case path.includes('/cloudfabric-locations/customer-premises/site-contacts') && httpMethod === 'GET':
      result = await routeHandler.getSiteContactsHandler(scsAuthToken, event, rc, consumerTypeFlag, csaObj);
      break;
    case path.includes('/cloudfabric-locations/customer-premises/site-contacts') && httpMethod === 'DELETE':
      result = await routeHandler.deleteSiteContactsHandler(scsAuthToken, event, rc, consumerTypeFlag, csaObj);
      break;


routhandler.ts
---------------	  
  public async getSiteContactsHandler(
    authToken: string,
    event: APIGatewayEvent,
    rc: RedisClientService,
    consumerTypeFlag: boolean,
    csaObj: RelatedPartyCustomerObj,
  ): Promise<ApiResponse> {
    logger.log('info', 'Start Get site contacts Request');

    if (consumerTypeFlag) {
      const genResponse = await rc.getValue('fabric-port-stub-getSiteContact');
      logger.log('info', `GetSiteContact: Stub Result ::::::::::  ${JSON.stringify(genResponse)}`);

      return {
        statusCode: HttpStatusCode.OK,
        body: JSON.stringify(genResponse),
      };
    }

    if (event.pathParameters?.contactId) {
      const { contactId } = event.pathParameters;
      logger.log('info', `Contact ID::::::::::  ${contactId}`);

      validateContactId(contactId);

      // Call MySite tmf669 for get contact role
      const tmf669ContactRoleResponse = await new ContactManagementService().getRole(authToken, csaObj.id, contactId);
      logger.log('info', `tmf669SiteContactResponse::::::: ${JSON.stringify(tmf669ContactRoleResponse)}`);

      // Call MySite tmf632 for get contact details
      const tmf632SiteContactResponse = await new ContactManagementService().getSiteContact(
        authToken,
        csaObj.id,
        contactId,
      );
      logger.log('info', `tmf632SiteContactResponse::::::: ${JSON.stringify(tmf632SiteContactResponse)}`);

      // Merge the data in to single customer response
      const siteContactsResponse = getSiteContactCusResponse(tmf632SiteContactResponse, tmf669ContactRoleResponse[0]);
      logger.log('info', `siteResponse::::::: ${JSON.stringify(siteContactResponse)}`);

      return {
        statusCode: HttpStatusCode.OK,
        body: JSON.stringify(siteContactsResponse),
      };
    }
    const reason: ErrorReason[] = [{ error: ErrorMessage.CONTACT_ID_EMPTY_ERROR }];
    throw new CloudFabricApplicationError('24', ErrorMessage.INVALID_BODY_FIELD, HttpStatusCode.BAD_REQUEST, reason);
  }

  public async deleteSiteContactsHandler(
    authToken: string,
    event: APIGatewayEvent,
    rc: RedisClientService,
    consumerTypeFlag: boolean,
    csaObj: RelatedParty,
  ): Promise<ApiResponse> {
    logger.log('info', `Request csaCusObj ::::::::::  ${JSON.stringify(csaObj)}`);

    // Pass pathParams and queryParams
    if (event.pathParameters?.siteId && event.pathParameters?.contactId && event.queryStringParameters?.contactRole) {
      const { siteId, contactId } = event.pathParameters;
      const contactRole = getQueryParameterFromEvent(event, AppConfigBtSites.CONTACT_ROLE);

      logger.log('info', `Site ID::::::::::  ${siteId}`);
      logger.log('info', `Contact ID::::::::::  ${contactId}`);
      logger.log('info', `ContactRole ID::::::::::  ${contactRole}`);

      logger.log('info', `Consumer Type Flag ::::::::::  ${consumerTypeFlag}`);

      let result = null;

      if (consumerTypeFlag) {
        result = await rc.getValue('fabric-port-stub-deleteSiteContact');
        logger.log('info', `Result ::::::::::  ${result}`);
      } else {
        validateRequestParams(siteId, contactId, contactRole);

        // Call MySite tmf669 for get contact role
        const tmf669ContactRoleResponse = await new ContactRoleManagementService().getPartyRole(
          authToken,
          siteId,
          contactId,
          contactRole,
          csaObj.id,
        );
        logger.log('info', `tmf669SiteContactResponse::::::: ${JSON.stringify(tmf669ContactRoleResponse)}`);

        if (tmf669ContactRoleResponse.length > 1) {
          const reason: ErrorReason[] = [];
          reason.push({
            error: ErrorMessage.CONTACT_ID_ERROR,
          });
          throw new CloudFabricApplicationError(
            '24',
            ErrorMessage.FETCH_CONTACT_ERROR,
            HttpStatusCode.BAD_REQUEST,
            reason,
          );
        }
        // Call MySite for Deletion
        await new ContactRoleManagementService().deleteSiteContact(
          authToken,
          tmf669ContactRoleResponse[0].id,
          csaObj.id,
        );
        logger.log('info', `tmf669SiteContactResponse::::::: ${JSON.stringify(tmf669ContactRoleResponse)}`);

        result = {
          code: HttpStatusCode.NO_CONTENT,
          reason: ReasonMessage.SUCCESS,
          message: `Contact Deleted Successfully with contactId ${contactId}`,
        };
      }

      return {
        statusCode: HttpStatusCode.NO_CONTENT,
        body: JSON.stringify(result),
      };
    }
    const reason: ErrorReason[] = [{ error: ErrorMessage.DELETE_CONTACT_ERROR }];
    throw new CloudFabricApplicationError('24', ErrorMessage.INVALID_BODY_FIELD, HttpStatusCode.BAD_REQUEST, reason);
  }
}

service.js
-----------
 public async getSiteContact(authToken: string, csaId: string, id: string) {
    logger.log('info', 'Start Get Site Contact::::');

    const fetchUrl = `${process.env.SCS_TMF_632_GET_URL}/${id}?customerServiceAccount=${csaId}&customerIdSource=MDM`;
    logger.log('info', `MySite TMF 632 fetchUrl::::::::::: ${JSON.stringify(fetchUrl)}`);

    const fetchOptions = {
      method: 'GET',
      headers: getHeadersWithSource(authToken),
    };

    logger.log('info', `MySite TMF 632 fetchOptions::::::::::: ${JSON.stringify(fetchOptions)}`);

    const response = await new FetchBuilderService()
      .useFetch(fetchUrl, fetchOptions)
      .useService(ApiService.SCS_AUTH)
      .useLogger(logger)
      .fetchBuild();

    logger.log('info', `MySite TMF 632 response ::::::::::  ${JSON.stringify(response)}`);

    return response as TMF632GetSiteContactResponse;
  }
  
  
 Interface.ts
---------------- 
  import { Characteristic } from './characteristic';

export interface TMF632GetSiteContactResponse {
  '@type': string;
  '@baseType': string;
  id: string;
  title: string;
  givenName: string;
  familyName: string;
  middleName: string;
  externalReference: ExternalReference[];
  contactMedium: ContactMediums[];
  partyCharacteristic: Characteristic[];
}

interface ExternalReference {
  name: string;
  externalReferenceType: string;
}

export interface ContactMediums {
  mediumType: string;
  characteristic: ContactMediumCharacteristic;
}

export interface ContactMediumCharacteristic {
  emailAddress?: string;
  phoneNumber?: string;
  phoneNumberExtension?: string;
  mobileNumber?: string;
  faxNumber?: string;
  pagerNumber?: string;
}

export enum MediumType {
  Email = 'email',
  TelephoneNumber = 'telephoneNumber',
  MobileNumber = 'mobileNumber',
  FaxNumber = 'faxNumber',
  PagerNumber = 'pagerNumber',
}


getSiteContactCusResponsehelper.ts
------------------------------------
export const getSiteContactCusResponse = (
  siteContact: TMF632GetSiteContactResponse,
  role: TMF669GetContactRoleResponse,
): GetSiteContactMSLResponse => {
  const siteIdChar = role.characteristic.filter((char) => char.name === AppConfigCreateSite.SITE_ID);
  let siteId = '';
  if (siteIdChar.length > 0) {
    siteId = siteIdChar[0].value ?? '';
  }

  const siteContactResponse: GetSiteContactMSLResponse = {
    contactId: siteContact.id,
    title: siteContact.title,
    givenName: siteContact.givenName,
    familyName: siteContact.familyName,
    email: getContactMediumCharacteristicValue(
      siteContact.contactMedium,
      MediumType.Email,
      AppConfigSiteContactConstants.EMAIL_ADDRESS,
    ),
    telephone: {
      number: getContactMediumCharacteristicValue(
        siteContact.contactMedium,
        MediumType.TelephoneNumber,
        AppConfigSiteContactConstants.PHONE_NUMBER,
      ),
      extension: getContactMediumCharacteristicValue(
        siteContact.contactMedium,
        MediumType.TelephoneNumber,
        AppConfigSiteContactConstants.PHONE_NUMBER_EXTENSION,
      ),
    },
    mobileNumber: getContactMediumCharacteristicValue(
      siteContact.contactMedium,
      MediumType.MobileNumber,
      AppConfigSiteContactConstants.MOBILE_NUMBER,
    ),
    siteIds: [
      {
        siteId,
        role: role.name,
      },
    ],
  };

  return siteContactResponse;
};